//main router

var jwt = require('jsonwebtoken');
var express = require('express');
var router = express.Router() ;

var mydb = require('../database/database_user_func.js') ;
var assetDb = require('../database/database_asset_func.js') ;

var query = require('../fabric/query.js');

//----------------- Server Config ------------------------
var serverConfig = require('../config-server.js') ;

// -------------------- Error Function -------------------
function getErrorMessage(field) {
	var response = {
		success: false,
		message: field + ' field is missing or Invalid in the request'
	};
	return response;
}

// ---------------- Start Route ---------------------------
router.use(function(req,res, next) {
  console.log('------------------------------ insid general -----------------------' );
  return next();
}) ;

router.post('/register', async function(req, res) {
  var response ;
  var username = req.body.username;
  var password = req.body.password;
  var email = req.body.email;
  var name = req.body.name;
  var familyname = req.body.familyname;
  var userType = req.body.userType ? req.body.userType : 'source' ;

  if (!username) {
    res.json(getErrorMessage('\'username\''));
    return;
  }
  if (!password) {
    res.json(getErrorMessage('\'password\''));
    return;
  }
  if (!email) {
    res.json(getErrorMessage('\'email\''));
    return;
  }
  if (!name) {
    res.json(getErrorMessage('\'name\''));
    return;
  }
  if (!familyname) {
    res.json(getErrorMessage('\'familyname\''));
    return;
  }
  if (!(userType == 'source' || userType == 'supplier' || userType == 'enduser' || userType == 'admin1' || userType == 'admin2')) {
    res.json(getErrorMessage('\'userType\''));
    return;
  }

  var dbresponse = await mydb.addUser(username, password, email, name, familyname, userType)
  if (dbresponse.success){
  response = {
    "success": true,
    "message": "register successful ",
  }
  }
  else{
  response = {
    "success": false,
    "message": dbresponse.message,
  }
  res.status(406)
  }
  res.json(response)
});

router.post('/login', async function(req, res) {
  var username = req.body.username;
  var password = req.body.password;
  var orgName =  req.body.orgName ? req.body.orgName : 'Org1' ;

  var  dbresponse = await mydb.checkUsernamePassword(username,password)
  var response = '';

  if (dbresponse.success) {
    var  dbresponse1 = await mydb.addLoginTime(username) ;
    user = dbresponse.user ;
    var token = jwt.sign({
      exp: Math.floor(Date.now() / 1000) + serverConfig.jwtExpireTime,
      username: username,
	    orgName: orgName,
      accountNumber: user.account_number,
      userType: user.user_type
    }, serverConfig.jwtSecret);

    var response = {
      "success": true,
      "message": "user login Successfully",
      "username": username,
	    "accountNumber" : user.account_number ,
	    "userType" : user.user_type ,
      "token": token
    }
    res.json(response)
  } else {
    console.log({
      username,
      password
    })
    var response = {
      "success": 'false',
      "message": dbresponse.message,
      "username": username,
    }
    res.json(response)
  }

});

//  Query Get Block by BlockNumber
router.get('/channels/:channelName/blocks/:blockId', async function(req, res) {
  try {
      logger.debug('==================== GET BLOCK BY NUMBER ==================');
      let blockId = req.params.blockId;
      let peer = req.query.peer;
      logger.debug('channelName : ' + req.params.channelName);
      logger.debug('BlockID : ' + blockId);
      logger.debug('Peer : ' + peer);
      if (!blockId) {
          res.json(getErrorMessage('\'blockId\''));
          return;
      }

      let message = await query.getBlockByNumber(peer, req.params.channelName, blockId, req.username, req.orgname);
      res.send(message);
  }
  catch (err){
      res.json(getErrorMessage('\'queryBlock(fabric.router)\''));
  }
});

// Query Get Transaction by Transaction ID
router.get('/channels/:channelName/transactions/:trxnId', async function(req, res) {
  try {
      logger.debug('================ GET TRANSACTION BY TRANSACTION_ID ======================');
      logger.debug('channelName : ' + req.params.channelName);
      let trxnId = req.params.trxnId;
      let peer = req.query.peer;
      if (!trxnId) {
          res.json(getErrorMessage('\'trxnId\''));
          return;
      }

      let message = await query.getTransactionByID(peer, req.params.channelName, trxnId, req.username, req.orgname);
      res.send(message);
  }
  catch (err){
      res.json(getErrorMessage('\'queryTx(fabric.router)\''));
  }
});

// Query Get Block by Hash
router.get('/channels/:channelName/blocks', async function(req, res) {
  logger.debug('================ GET BLOCK BY HASH ======================');
  logger.debug('channelName : ' + req.params.channelName);
  let hash = req.query.hash;
  let peer = req.query.peer;
  if (!hash) {
    res.json(getErrorMessage('\'hash\''));
    return;
  }

  let message = await query.getBlockByHash(peer, req.params.channelName, hash, req.username, req.orgname);
  res.send(message);
});

//Query for Channel Information
router.get('/channels/:channelName', async function(req, res) {
  logger.debug('================ GET CHANNEL INFORMATION ======================');
  logger.debug('channelName : ' + req.params.channelName);
  let peer = req.query.peer;

  let message = await query.getChainInfo(peer, req.params.channelName, req.username, req.orgname);
  res.send(message);
});

//Query for Channel instantiated chaincodes
router.get('/channels/:channelName/chaincodes', async function(req, res) {
  logger.debug('================ GET INSTANTIATED CHAINCODES ======================');
  logger.debug('channelName : ' + req.params.channelName);
  let peer = req.query.peer;

  let message = await query.getInstalledChaincodes(peer, req.params.channelName, 'instantiated', req.username, req.orgname);
  res.send(message);
});

// Query to fetch all Installed/instantiated chaincodes
router.get('/chaincodes', async function(req, res) {
  var peer = req.query.peer;
  var installType = req.query.type;
  logger.debug('================ GET INSTALLED CHAINCODES ======================');

  let message = await query.getInstalledChaincodes(peer, null, 'installed', req.username, req.orgname)
  res.send(message);
});

// Query to fetch channels
router.get('/channels', async function(req, res) {
  logger.debug('================ GET CHANNELS ======================');
  logger.debug('peer: ' + req.query.peer);
  var peer = req.query.peer;
  if (!peer) {
    res.json(getErrorMessage('\'peer\''));
    return;
  }

  let message = await query.getChannels(peer, req.username, req.orgname);
  res.send(message);
});



router.post('/loginMicro',async function(req, res){
	try {
    console.log('at beggining of of loginMicro api');
    
    var microId = req.body.microId;
    var password = req.body.password;
    var orgName = req.body.orgname;

    console.log('loading body of request in moginMicro api');

    var dbResponse = await assetDb.findAssetsOnlyByMicroId(microId);
    var serialNumber = dbResponse.assets.serial_number;
    console.log('request (findAssetsOnlyByMicroId) to asset data base in loginMicro api') ;
    if (dbResponse.assetCount != 1){
      var response = {
        "success": false,
        "message": 'There is more than a micro with this Id',
      }
       res.send(response) ;
       return ;
    }
    else {
      if (dbResponse.assets.micro_password == null) {
        var dbResponseSetPass = await assetDb.microSetPass(microId, password);
        console.log('request (microSetPass) to asset data base in loginMicro api') ;
        if (!dbResponseSetPass.success) {
          var response = {
            "success": false,
            "message": 'can not set password for micro',
          }
           res.send(response) ;
           return ;
        }
        else {
          var dbResponseLoginTime = await assetDb.microLoginTime(microId);
          console.log('request (microLoginTime) to asset data base in loginMicro api') ;
          var token = jwt.sign({
            exp: Math.floor(Date.now() / 1000) + serverConfig.jwtExpireTime,
            microId: microId,
            orgName: orgName,
            userType: "Micro"
          }, serverConfig.jwtSecret);
      
          var response = {
            "success": true,
            "message": "micro login Successfully",
            "microId": microId,
            "serialNumber" : serialNumber ,
            "userType" : "Micro" ,
            "token": token
          }
          res.send(response) ;
          return ;
        }
      }
      else {
        var dbResponseLoginCheck = await assetDb.microLogin(microId, password);
        console.log('request (microLogin) to asset data base in loginMicro api') ;
        if (!dbResponseLoginCheck.success) {
          var response = {
            "success": false,
            "message": 'can not login for micro',
          }
           res.send(response) ;
           return ;
        }
        else {
          var dbResponseLoginTime = await assetDb.microLoginTime(microId);
          console.log('request (microLoginTime) to asset data base in loginMicro api') ;
          var token = jwt.sign({
            exp: Math.floor(Date.now() / 1000) + serverConfig.jwtExpireTime,
            microId: microId,
            orgName: orgName,
            userType: "Micro"
          }, serverConfig.jwtSecret);
      
          var response = {
            "success": true,
            "message": "micro login Successfully",
            "microId": microId,
            "serialNumber" : serialNumber ,
            "userType" : "Micro" ,
            "token": token
          }
          res.send(response) ;
          return ;
        }
      }
    }
    
	}
	catch(err){
		var response = {
		 "success": false,
		 "message": 'Micro login was failed',
	   }
		res.send(response) ;
	}
});







module.exports = router
