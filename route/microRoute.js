'use strict';
// -------------------- Setting ------------------------------
var log4js = require('log4js');
var logger = log4js.getLogger('UserRoute');
var express = require('express') ;
var router = express.Router()  ;

// -------------------- Database -----------------------------
var assetDb = require('../database/database_asset_func.js');

// --------------------- Fabric -------------------------------
var invoke = require('../fabric/invoke-transaction.js');
var query = require('../fabric/query.js');
var helper = require('../fabric/helper.js');
var helperCA = require('../fabric/helper-CA.js');

// -------------------- Error Function -------------------
function getErrorMessage(field) {
	var response = {
		success: false,
		message: field + ' field is missing or Invalid in the request'
	};
	return response;
}

// ---------------------Start Route------------------------
router.use(function(err ,req, res, next) {
  console.log('Time: ', Date.now())
  console.log(req.username);
  console.log('----------------------------- Insid User Route -----------------------' );
  return next();
})





router.post('/GetMicroAsset',async function(req, res){
	try {
		
		
	}
	catch(err){
		var response = {
		 "success": false,
		 "message": 'invoke Failed ...',
	   }
		res.send(response) ;
	}
});


router.post('/changeProp',async function(req, res){
  try {

    logger.debug('==================== INVOKE ON CHAINCODE TO CHANGE PROPERTIES (FOR MICRO) ==================');
    var serialNumber = req.body.serialNumber;
    var newTemp = req.body.newTemp ;
    var newVar = req.body.newVar ;
    var requestedPeers = req.body.peers ;

    if (!serialNumber) {
      res.json(getErrorMessage('\'serialNumber\''));
      return;
    }

    if (!newHolder) {
      res.json(getErrorMessage('\'newHolder\''));
      return;
    }

    var args = [serialNumber,newTemp, newVar];
    var fcn = 'changeProperties';
    var chaincodeName = 'mycc';
    var channelName = 'mychannel';
    var peers = requestedPeers ? requestedPeers : ["peer0.org1.example.com","peer0.org2.example.com"];

    logger.debug('channelName  : ' + channelName);
    logger.debug('chaincodeName : ' + chaincodeName);
    logger.debug('fcn  : ' + fcn);
    logger.debug('args  : ' + args);
    logger.debug('peer  : ' + peers);
    let message = await invoke.invokeChaincode(peers, channelName, chaincodeName, fcn, args, req.username, req.orgname);
    res.send(message);
  }
  catch (err) {
    var response = {
     "success": false,
     "message": 'invoke Failed for chainging properties by micro',
   }
    res.send(response) ;
  }

});


module.exports = router
