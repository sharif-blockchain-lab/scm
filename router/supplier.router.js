var log4js = require('log4js');
var logger = log4js.getLogger('SampleWebApp');
var express = require('express');
var session = require('express-session');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var http = require('http');
var util = require('util');
var app = express();
var expressJWT = require('express-jwt');
var jwt = require('jsonwebtoken');
var bearerToken = require('express-bearer-token');
var cors = require('cors');

var hfc = require('fabric-client');

var helper = require('../fabric/helper.js');
var createChannel = require('../fabric/create-channel.js');
var join = require('../fabric/join-channel.js');
var updateAnchorPeers = require('../fabric/update-anchor-peers.js');
var install = require('../fabric/install-chaincode.js');
var instantiate = require('../fabric/instantiate-chaincode.js');
var invoke = require('../fabric/invoke-transaction.js');
var query = require('../fabric/query.js');
var host = process.env.HOST || hfc.getConfigSetting('host');
var port = process.env.PORT || hfc.getConfigSetting('port');




async function invokeSupplier(req, res) {
  logger.debug('==================== INVOKE ON CHAINCODE (FOR SUPPLIER) ==================');
  var peers = req.body.peers;
  var chaincodeName = req.params.chaincodeName;
  var channelName = req.params.channelName;
  var fcn = req.body.fcn;
  var args = req.body.args;
  logger.debug('channelName  : ' + channelName);
  logger.debug('chaincodeName : ' + chaincodeName);
  logger.debug('fcn  : ' + fcn);
  logger.debug('args  : ' + args);
  if (!chaincodeName) {
    res.json(getErrorMessage('\'chaincodeName\''));
    return;
  }
  if (!channelName) {
    res.json(getErrorMessage('\'channelName\''));
    return;
  }
  if (!fcn) {
    res.json(getErrorMessage('\'fcn\''));
    return;
  }
  if (!args) {
    res.json(getErrorMessage('\'args\''));
    return;
  }

  let message = await invoke.invokeChaincode(peers, channelName, chaincodeName, fcn, args, req.username, req.orgname);
  res.send(message);
}


async function registerSuppier(req, res){
  try {
    logger.debug('====================  REGISTER SUPPLIER ==================');
    users.push(req.body) ;
    console.log(JSON.stringify(req.body))
    var response = {
      "success": true,
      "message": "register successful ",
    }
    res.json(response);
  }
  catch (err){
    res.json(getErrorMessage('\'registerSupplier(suppplier.router)\''));
  }
}

async function loginSupplier(req, res){
  try {
    var username = req.body.username ;
    var pass = req.body.pass ;
    var user =  '' ;
    user = users.find(user => user.name == username && user.pass == pass)
    var response= '' ;
    if (user){
      var token = jwt.sign({
        exp: Math.floor(Date.now() / 1000) + 100,
        username: username,
        orgName: 'Micro' ,
      }, app.get('secret'));

      var  response = {
        "success": true,
        "message": "user login Successfully",
        "username": user.name,
        "token" : token
      }
      res.json(response)
      console.log("ok" + response );
    }
    else {
      console.log({username , pass})
      var response = {
        "success": false,
        "message": "error login ",
        "username": username,
      }
      res.json(response)
    }
  }
  catch (err){
    res.json(getErrorMessage('\'loginSupplier(supplier.router)\''));
  }
}


exports.invokeSupplier = invokeSupplier ;

exports.registerSupplier = registerSuppier;

exports.loginSupplier = loginSupplier;
