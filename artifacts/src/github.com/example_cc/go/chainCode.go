package main

import (

	"encoding/json"
	"fmt"
	"strconv"
	"bytes"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

//this function is a test function that returns the Type of the user who sent the TX
// @Param APIstub
// @Param args, the argument array containing senderAccountNumber
// @return A response message indicating the Type of the sender as SUPPLIER, SOURCE, etc

func (s *SmartContract) who (APIstub shim.ChaincodeStubInterface, args []string) peer.Response {

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments, expecting 1")
	}

	senderAccountNumber := args[0]

	// Checking sender account existence
	sender, err := getUser(APIstub, senderAccountNumber)
	if err != nil || sender == (User{}) {
		return shim.Error("Sender account doesn't exist.")
	}

	return shim.Success([]byte(fmt.Sprintf("========Type is: %s ",sender.Type)))
}


//this function is a test function returns the current holder and location of a micro
// @Param APIstub
// @Param args, the argument array containing assetSerialNumber
// @return A response message indicating the Type of the sender as SUPPLIER, SOURCE, etc

func (s *SmartContract) what (APIstub shim.ChaincodeStubInterface, args []string) peer.Response {

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments, expecting 1")
	}

	assetSerialNumber := args[0]

	// Checking if the micro exists
	asset, err := getAsset(APIstub, assetSerialNumber)
	if err != nil || asset == (Asset{}) {
		return shim.Error("Asset account doesn't exist.")
	}

	return shim.Success([]byte(fmt.Sprintf("Asset info : %s,%s ",asset.Holder,asset.Location)))
}

//this function is used for testing the chainCode
// @Param APIstub
// @Param args, the argument array containing senderAccountNumber and a value
// @return A that prints the value and returns the accountnumber if the sender exists

func (s *SmartContract) test(APIstub shim.ChaincodeStubInterface, args []string) peer.Response {

	if len(args) != 2 {
		return shim.Error("Incorrect number of arguments, expecting 2")
	}

	senderAccountNumber := args[0]
	valueStr := args[1]

	// Checking sender account existence
	sender, err := getUser(APIstub, senderAccountNumber)
	if err != nil || sender == (User{}) {
		return shim.Error("Sender account doesn't exist.")
	}

	return shim.Success([]byte(fmt.Sprintf("Attribute value is: %s \n requestSender is: %s", valueStr , sender.AccountNumber)))
}


// This function only allows SOURCES to add their asset on the ledger!
// @param APIstub
// @return A response structure indicating success or failure with a message
// @param args
// What are the inputs?
// ObjectType == AssetObjectType
// arg[0] = SerialNumber
// arg[1] = AssetType
// arg[2] = AssetVar
/// Status == false

func (s *SmartContract) confirmAsset(APIstub shim.ChaincodeStubInterface, requestSender User, args []string) peer.Response {


	if requestSender.Type != SOURCE {  // Org1 must
		return shim.Error("Only sources can add asset to the ledger!")
	}
	if len(args) != 3 {
		return shim.Error("Incorrect number of arguments, expecting 3")
	}

	serialNumber := args[0]
	assetType := args[1]
	assetVar := args[2]

	asset, err := getAsset(APIstub, serialNumber)

	if err != nil || asset == (Asset{}) {
		return shim.Error("Asset doesn't exist.")
	}
	if asset.Status == true {
		return shim.Error("Asset can not be confirmed, it's already confirmed and even reaches to the destination.")
	}


	if requestSender.AccountNumber != asset.Holder {
		return shim.Error("Only holder can add asset to the ledger!")
	}


	asset.AssetType = assetType
	asset.Variable = assetVar

	assetJsonAsBytes, err := json.Marshal(asset)
	if err!= nil {
		return shim.Error(err.Error())
	}
	err = APIstub.PutState(serialNumber, assetJsonAsBytes)

	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to confirming asset in chaincode %s, %s",serialNumber, err.Error()))
	}

	return shim.Success([]byte(fmt.Sprintf("Successfully confirmed asset in chaincode, Serial. No.: %s", serialNumber)))

}



// This function only allows Assets to change the properties on the ledger!
// @param APIstub
// @return A response structure indicating success or failure with a message
// @param args
// What are the inputs?
// arg[0] = new_location
// arg[1] = new_Temp
// arg[2] = new_variable
func (s *SmartContract) changeProperties (APIstub shim.ChaincodeStubInterface, requestAsset Asset, args []string) peer.Response {

	if len(args) != 3 {
		return shim.Error("Incorrect number of arguments, expecting 3")
	}

	requestAsset.Location = args[0]

	assetTemperetaure, convErrAssetTemp := strconv.ParseUint(args[1], 10, 64)

	if convErrAssetTemp != nil {
		return shim.Error(fmt.Sprintf("Provided Tempretuare was not a number: %s",
			convErrAssetTemp.Error()))
	}

	requestAsset.Temperature = assetTemperetaure
	requestAsset.Variable = args[2]


	assetJsonAsBytes, err := json.Marshal(requestAsset)
	if err!= nil {
		return shim.Error(err.Error())
	}
	err = APIstub.PutState(requestAsset.SerialNumber, assetJsonAsBytes)

	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to change properties of asset with serial number %s, %s",requestAsset.SerialNumber, err.Error()))
	}

	return shim.Success([]byte(fmt.Sprintf("Successfully changed the properties of asset, Serial. No.: %s", requestAsset.SerialNumber)))
}

// This function only allows the holder of an asset to change the holder,
// @param APIstub
// @return A response structure indicating success or failure with a message
// @param args
// What are the inputs?
//arg[0] = serial number
//arg[1] = new_holder (account #)
func (s *SmartContract) changeHolder (APIstub shim.ChaincodeStubInterface, requestSender User, args []string) peer.Response {

	if len(args) != 2 {
		return shim.Error("Incorrect number of arguments, expecting 2")
	}

	serialNumber := args[0]
	newHolder := args[1]

	asset, err := getAsset(APIstub, serialNumber)

	if err != nil || asset == (Asset{}) {
		return shim.Error("Asset doesn't exist.")
	}
	if asset.Status == true {
		return shim.Error("Asset can not be transferred. It has already reached the destination.")
	}

	if requestSender.AccountNumber != asset.Holder {
		return shim.Error("Only holder can change asset holder !")
	}

	receiver, err1 := getUser(APIstub, newHolder)

	if err1 != nil || receiver == (User{}) {
		return shim.Error("Receiver doesn't exist.")
	}
	if receiver.Type == ENDUSER && (requestSender.Type == SUPPLIER || requestSender.Type == SOURCE) {
		asset.Status = true
	} // we do not want end users to be able to sell the product

	asset.Holder = newHolder

	assetJsonAsBytes, err := json.Marshal(asset)
	if err!= nil {
		return shim.Error(err.Error())
	}
	err = APIstub.PutState(serialNumber, assetJsonAsBytes)

	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to change holder of asset with serial number %s, %s",serialNumber, err.Error()))
	}

	return shim.Success([]byte(fmt.Sprintf("Successfully changed the holder of asset, Serial. No.: %s", serialNumber)))
}

// This function only allows the holder of an asset to change the status,
// MERCHANTs are the only holders allowed to change the status (for example in case that the fish is decayed)
// @param APIstub
// @return A response structure indicating success or failure with a message
// @param args
// What are the inputs?
//arg[0] = serial number
func (s *SmartContract) changeStatus (APIstub shim.ChaincodeStubInterface, requestSender User, args []string) peer.Response {

	if len(args) !=  1{
		return shim.Error("Incorrect number of arguments, expecting 1")
	}

	serialNumber := args[0]

	asset, err := getAsset(APIstub, serialNumber)

	if err != nil || asset == (Asset{}) {
		return shim.Error("Asset doesn't exist.")
	}

	if asset.Status == true {
		return shim.Error("The asset has already reached the destination.")
	}

	if asset.Holder != requestSender.AccountNumber || requestSender.Type != SOURCE || requestSender.Type != SUPPLIER  {
		return shim.Error("You are not allowed to change the status of this asset.")
	}

	asset.Status = true

	assetJsonAsBytes, err := json.Marshal(asset)
	if err!= nil {
		return shim.Error(err.Error())
	}
	err = APIstub.PutState(serialNumber, assetJsonAsBytes)

	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to change status of asset with serial number %s, %s",serialNumber, err.Error()))
	}

	return shim.Success([]byte(fmt.Sprintf("Successfully changed the status of asset, Serial. No.: %s", serialNumber)))
}

// This is an only admin function
// @param APIstub
// @param args The arguments array containing accountNumber to be deleted
// @return A response structure indicating success or failure with a message
// Admins can not delete themselves or other admins!
func (s *SmartContract) deleteUser (APIstub shim.ChaincodeStubInterface, requestSender User, args []string) peer.Response {

	if requestSender.Type != AdminOrg2 && requestSender.Type != AdminOrg1 {
		return shim.Error("Only admins can delete users.")
	}

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments, expecting 1")
	}

	accountNumber := args[0]

	user, err := getUser(APIstub, accountNumber)
	if err != nil || user == (User{}) {
		return shim.Error("User doesn't exist.")
	}

	if user.Type == AdminOrg1 || user.Type == AdminOrg2 {
		return shim.Error("No one can delete admins.")
	}
	// first we transferPrune existing deltas
	pruneResp, deltaSum := transferPrune(APIstub, &requestSender, true)
	if pruneResp.Status == ERROR {
		return shim.Error(fmt.Sprintf(
			"Could not transferPrune account deltas, deleting canceled. Try again. Error: %s", pruneResp.Message))
	}

	deltaSum += user.Balance

	// now we delete the account
	delErr := APIstub.DelState(accountNumber)
	if delErr != nil {
		return shim.Error(fmt.Sprintf("Could not delete the account %s: \n %s", accountNumber, delErr.Error()))
	}

	return shim.Success([]byte(fmt.Sprintf("Successfully deleted account, Acc. No.: %s and balance of the account is %d", accountNumber, deltaSum)))
}


// the function below returns all of the current key-value pairs giving start key and end key
// @param APIstub
// @param args The arguments array containing startKey, endKey
// @return A response string
//startKey := args[0]
//endKey := args[1]
func (s *SmartContract) getAllAssets (APIstub shim.ChaincodeStubInterface, requestSender User, args []string) peer.Response {

	if len(args) != 2 {
		return shim.Error("Incorrect number of arguments. Expecting 2")
	}

	startKey := args[0]
	endKey := args[1]

	iterator, err := APIstub.GetStateByRange(startKey, endKey)
	if err != nil {
		return shim.Error(fmt.Sprintf("keys operation failed. Error accessing state: %s", err))
	}
	defer iterator.Close()

	if err != nil {
		return shim.Error(err.Error())
	  }
	  defer iterator.Close()
	
	
	  var buffer bytes.Buffer
	  buffer.WriteString("[")
	
	  flag := false
	  for iterator.HasNext() {
		queryResponse, err := iterator.Next()
		if err != nil {
		  return shim.Error(err.Error())
		}
	
		if flag == true {
		  buffer.WriteString(",")
		}
	
		// constructing JSOn files key/value pairs
		buffer.WriteString("{\"Key\":")
		buffer.WriteString("\"")
		buffer.WriteString(string(queryResponse.Key))
		buffer.WriteString("\"")
	
		buffer.WriteString(", \"Value\":")
		buffer.WriteString(string(queryResponse.Value))
		buffer.WriteString(", \"Timestamp\":")
		//buffer.WriteString(strconv.FormatInt(queryResponse.Timestamp.Seconds,10))
		//buffer.WriteString("}")
		flag = true
	  }
	  buffer.WriteString("]")
	
	  //for debug purposes
	  fmt.Printf("- queryAllAsset:\n%s\n", buffer.String())
	
	  return shim.Success(buffer.Bytes())
	
}

// the function below returns all of the transaction history for one asset
// @param APIstub
// @param args The arguments array containing serialNumber
// @return A response string
//serialNumber := args[0]
func (s *SmartContract) getOneAssetHistory (APIstub shim.ChaincodeStubInterface, requestSender User, args []string) peer.Response {

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	serialNumber := args[0]
	asset, err := getAsset(APIstub, serialNumber)
	// check whether the invoker is admin
	// or if user, check whether she has the access
	//if requestSender.Type != AdminOrg1 {
	//	return shim.Error("user not authorized")
	//}
	if err != nil || asset == (Asset{}) {
		return shim.Error("Asset doesn't exist.")
	}
	if asset.Status == true {
		return shim.Error("Asset can not be transferred. It has already reached the destination.")
	}
	iterator, err := APIstub.GetHistoryForKey(serialNumber)
	if err != nil {
		return shim.Error(err.Error())
	  }
	  defer iterator.Close()
	
	
	  var buffer bytes.Buffer
	  buffer.WriteString("[")
	
	  flag := false
	  for iterator.HasNext() {
		queryResponse, err := iterator.Next()
		if err != nil {
		  return shim.Error(err.Error())
		}
	
		if flag == true {
		  buffer.WriteString(",")
		}
	
		// constructing JSOn files key/value pairs
		buffer.WriteString("{\"Key\":")
		buffer.WriteString("\"")
		buffer.WriteString(serialNumber)
		buffer.WriteString("\"")
	
		buffer.WriteString(", \"Value\":")
		buffer.WriteString(string(queryResponse.Value))
		buffer.WriteString(", \"Timestamp\":")
		buffer.WriteString(strconv.FormatInt(queryResponse.Timestamp.Seconds,10))
		buffer.WriteString("}")
		flag = true
	  }
	  buffer.WriteString("]")
	
	  //for debug purposes
	  fmt.Printf("- queryAllAsset:\n%s\n", buffer.String())
	
	  return shim.Success(buffer.Bytes())
	
	//Also note, that you need to make sure history db is enabled, in core.yaml file part of the ledger section:
	//ledger:
	// history:
	//# enableHistoryDatabase - options are true or false
	//# Indicates if the history of key updates should be stored.
	//# All history 'index' will be stored in goleveldb, regardless if using
	//# CouchDB or alternate database for the state.
	//	enableHistoryDatabase: true


}


// The two following fuctions are not used in the current version
func (s *SmartContract) getOneAssetHistory_old (APIstub shim.ChaincodeStubInterface, requestSender User, args []string) peer.Response {

	if len(args) != 2 {
		return shim.Error("Incorrect number of arguments. Expecting 2")
	}

	pageNumberStr := args[0]
	limitStr := args[1]

	// check whether the invoker is admin
	// or if user, check whether she has the access
	if requestSender.Type != AdminOrg1 {
		return shim.Error("user not authorized")
	}

	pageNumberInt, convErrPageNumber := strconv.ParseUint(pageNumberStr, 10, 64)

	if convErrPageNumber != nil {
		return shim.Error(fmt.Sprintf("Provided pageNumber was not an unsigned integer number: %s",
			convErrPageNumber.Error()))
	}

	limitInt, convErrLimit := strconv.ParseUint(limitStr, 10, 64)

	if convErrLimit != nil{
		return shim.Error(fmt.Sprintf(
			"Provided limit was not an unsigned integer number: %s", convErrLimit.Error()))
	}

	skip := (pageNumberInt - 1) * limitInt

	queryString := fmt.Sprintf(
		"{\"selector\":" +
			"{\"docType\":\"transaction\" }," +
			"\"fields\":[\"sender\", \"receiver\" , \"value\", \"description\", \"timestamp\"]," +
			" \"sort\":[{\"timestamp\":\"desc\"}]," +
			"\"limit\":\"%s\"," +
			"\"skip\":\"%d\"" +
			"}",
		limitStr, skip)

	queryResults, err := getQueryResultForQueryString(APIstub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResults)
}
// The following function is not used in the current version
func (s *SmartContract) getAllAssetHistory_old (APIstub shim.ChaincodeStubInterface, requestSender User, args []string) peer.Response {

	if len(args) != 2 {
		return shim.Error("Incorrect number of arguments. Expecting 2")
	}

	pageNumberStr := args[0]
	limitStr := args[1]

	// check whether the invoker is admin
	// or if user, check whether she has the access
	if requestSender.Type != AdminOrg1 {
		return shim.Error("user not authorized")
	}

	pageNumberInt, convErrPageNumber := strconv.ParseUint(pageNumberStr, 10, 64)

	if convErrPageNumber != nil {
		return shim.Error(fmt.Sprintf("Provided pageNumber was not an unsigned integer number: %s",
			convErrPageNumber.Error()))
	}

	limitInt, convErrLimit := strconv.ParseUint(limitStr, 10, 64)

	if convErrLimit != nil{
		return shim.Error(fmt.Sprintf(
			"Provided limit was not an unsigned integer number: %s", convErrLimit.Error()))
	}

	skip := (pageNumberInt - 1) * limitInt

	queryString := fmt.Sprintf(
		"{\"selector\":" +
			"{\"docType\":\"transaction\" }," +
			"\"fields\":[\"sender\", \"receiver\" , \"value\", \"description\", \"timestamp\"]," +
			" \"sort\":[{\"timestamp\":\"desc\"}]," +
			"\"limit\":\"%s\"," +
			"\"skip\":\"%d\"" +
			"}",
		limitStr, skip)

	queryResults, err := getQueryResultForQueryString(APIstub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResults)
}